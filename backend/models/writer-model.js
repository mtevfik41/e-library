import mongoose from 'mongoose';

const writerSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
});

const Writer = mongoose.model('Writer', writerSchema);

export default Writer;
