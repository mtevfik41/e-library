import asyncHandler from 'express-async-handler';
import stripe from 'stripe';

// @description     Get stripe secret
// @route           POST /api/orders/config/stripe-sk
// @access          Private
const getStripeSecret = asyncHandler(async (req, res) => {
  try {
    const paymentIntent = await stripe(process.env.STRIPE_TEST_SECRET).paymentIntents.create({
      amount: 100,
      currency: req.body.currency,
      metadata: { integration_check: 'accept_a_payment' },
    });

    res.json({ client_secret: paymentIntent.client_secret });
  } catch (error) {
    console.log(error);
    res.status(500);
    throw new Error("STRIPE API'ye bağlanılamıyor");
  }
});

// @description     Get stripe pk
// @route           GET /api/orders/config/stripe-pk
// @access          Private
const getStripePublicKey = asyncHandler(async (req, res) => {
  try {
    res.json({ public_key: process.env.STRIPE_TEST_PUBLIC_KEY });
  } catch (error) {
    console.log(error);
    res.status(500);
    throw new Error("STRIPE API public key'e ulaşılamıyor");
  }
});

export { getStripeSecret, getStripePublicKey };
