import sgMail from '@sendgrid/mail';
import dotenv from 'dotenv';

dotenv.config();

sgMail.setApiKey(process.env.SENGRID_API_KEY);

const sendWelcomeEmail = (email, name) => {
  sgMail.send({
    to: email,
    from: 'mtevfik41@gmail.com',
    subject: "E-Library'e hoşgeldiniz!",
    text: `E-Library\'e hoşgeldiniz, ${name}. Bu emaili görüyorsanız emailiniz onaylanmıştır.`,
  });
};

const sendForgotPasswordEmail = (email, token) => {
  sgMail.send({
    to: email,
    from: 'mtevfik41@gmail.com',
    subject: "Şifremi Unuttum",
    text: `Yeni şifreniz: ${token}`
  })
}

export { sendWelcomeEmail, sendForgotPasswordEmail };
