import jwt from 'jsonwebtoken';

// user id'ye göre token jwt kullanılarak token üretimi
const generateToken = id => {
  return jwt.sign({ id }, process.env.JWT_SECRET, { expiresIn: '30d' });
};

export default generateToken;
