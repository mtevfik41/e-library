

## Yükleme

ilk önce ana dizinde

```bash
npm install
```

komutunu çalıştırıyoruz ardından frontend klasörünün içinde 

```bash
npm install
```

komutunun çalıştırıyoruz.

### Uygulamayı çalıştırmak için

backend ve frontendi aynı anda çalıştırmak için concurrently paketini kullandım.

ana dizinde

```bash
npm run dev
``` 

scriptini çalıştırıyoruz.

Projemiz http://localhost:3000/ de aktif.
