import React from 'react';
import { shallow } from 'enzyme';

import App from '../../src/App';

import Header from '../../src/components/Header';
import Footer from '../../src/components/Footer';

let wrapper;

beforeEach(() => {
  wrapper = shallow(<App />);
});

describe('App page', () => {
  describe('Smoke tests', () => {
    it('Should has one Header instance', () => {
      expect(wrapper.find(Header).length).toEqual(1);
    });

    it('Should has one Footer instance', () => {
      expect(wrapper.find(Footer).length).toEqual(1);
    });
  });
});
