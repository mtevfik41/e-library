import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Footer = () => {
  return (
    <footer>
      <Container>
        <Row>
          <Col className="text-center py-3">&copy; E-Library</Col>
          <Col className="text-center py-3">
            <a href="mailto:mtevfik41@gmail.com">İletişim</a>
          </Col>
        </Row>
      </Container>
    </footer>
  );
};

export default Footer;
