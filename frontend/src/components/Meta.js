import React from 'react';
import { Helmet } from 'react-helmet';

const Meta = ({ title, description, keywords }) => {
  return (
    <Helmet>
      <title>{title}</title>
      <meta name="description" content={description} />
      <meta name="keywords" content={keywords} />
    </Helmet>
  );
};

Meta.defaultProps = {
  title: 'E-Library',
  description: 'En ucuz ve en iyi kitapların yeri',
  keywords: 'Ucuz kitap, kitap',
};

export default Meta;
