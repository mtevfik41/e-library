import React, { useEffect, useState, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Row, Col, Form, Button } from 'react-bootstrap';
import Product from '../components/Product';
import Message from '../components/Message';
import Loader from '../components/Loader';
import Paginate from '../components/Paginate';
import ProductCarousel from '../components/ProductCarousel';
import Meta from '../components/Meta';
import { listProducts } from '../actions/product-actions';

const HomeScreen = ({ match, history }) => {
  const keyword = match.params.keyword;

  const pageNumber = match.params.pageNumber || 1;

  const dispatch = useDispatch();

  const productList = useSelector(state => state.productList);
  const { loading, error, products, page, pages } = productList;

  useEffect(() => {
    dispatch(listProducts(keyword, pageNumber));
  }, [dispatch, keyword, pageNumber, listProducts]);
  const mostSelledProducts = () => {
    dispatch(listProducts(keyword, pageNumber, true));
  };
  const [category, setCategory] = useState('');
  const submitHandler = e => {
    e.preventDefault();

    if (category.trim()) {
      history.push(`/search/${category}`);
    } else {
      history.push('/');
    }
  };
  console.log(products);
  return (
    <>
      <Meta />
      {!keyword ? (
        <ProductCarousel />
      ) : (
        <Link to="/" className="btn btn-light">
          Geri Dön
        </Link>
      )}
      <h1>Son ürünler</h1>
      {loading ? (
        <Loader />
      ) : error ? (
        <Message variant="danger">{error}</Message>
      ) : (
        <>
          <Fragment>
            <Button onClick={mostSelledProducts} className="btn-sm">
              Çok satanlar
            </Button>
            <Form onSubmit={submitHandler}>
              <Form.Label>Filtre</Form.Label>
              <Form.Control as="select" onChange={e => setCategory(e.target.value)} value={category} className="flex-end">
                <option value="" disabled>
                  Bir Kategori Seçiniz
                </option>
                <option value="Anı">Anı</option>
                <option value="Anlatı">Anlatı</option>
                <option value="Çizgi Roman">Çizgi Roman</option>
                <option value="Deneme">Deneme</option>
                <option value="Destan">Destan</option>
                <option value="Eleştiri">Eleştiri</option>
                <option value="Halk Edebiyatı">Halk Edebiyat</option>
                <option value="Roman">Roman</option>
                <option value="Hiciv">Hiciv</option>
              </Form.Control>
              <Button type="submit" variant="outline-success" className="m-2 ml-0">
                Ara
              </Button>
            </Form>
          </Fragment>

          <Row>
            {!loading &&
              products.map(product => (
                <Col key={product._id} sm={12} md={6} lg={4} xl={3}>
                  <Product product={product} />
                </Col>
              ))}
          </Row>
          <Paginate pages={pages} page={page} keyword={keyword ? keyword : ''} />
        </>
      )}
    </>
  );
};

export default HomeScreen;
