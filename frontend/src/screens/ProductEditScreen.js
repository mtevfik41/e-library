import axios from 'axios';
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Form, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import Message from '../components/Message';
import Loader from '../components/Loader';
import Meta from '../components/Meta';
import FormContainer from '../components/FormContainer';
import { listProductDetails, updateProduct } from '../actions/product-actions';
import { PRODUCT_UPDATE_RESET } from '../constants/product-constants';

const ProductEditScreen = ({ match, history }) => {
  const productId = match.params.id;

  const [name, setName] = useState('');
  const [price, setPrice] = useState(0);
  const [image, setImage] = useState('');
  const [brand, setBrand] = useState('');
  const [category, setCategory] = useState('');
  const [countInStock, setCountInStock] = useState(0);
  const [description, setDescription] = useState('');
  const [writer, setWriter] = useState('');
  const [uploading, setUploading] = useState(false);

  const dispatch = useDispatch();

  const productDetails = useSelector(state => state.productDetails);
  const { loading, error, product } = productDetails;

  const productUpdate = useSelector(state => state.productUpdate);
  const { loading: loadingUpdate, error: errorUpdate, success: successUpdate } = productUpdate;

  useEffect(() => {
    if (successUpdate) {
      dispatch({ type: PRODUCT_UPDATE_RESET });
      history.push('/admin/productlist');
    } else {
      if (!product.name || product._id !== productId) {
        dispatch(listProductDetails(productId));
      } else {
        setName(product.name);
        setPrice(product.price);
        setImage(product.image);
        setBrand(product.brand);
        setCategory(product.category);
        setCountInStock(product.countInStock);
        setDescription(product.description);
        setWriter(product.writer);
      }
    }
  }, [dispatch, history, productId, product, successUpdate]);

  const uploadFileHandler = async e => {
    const file = e.target.files[0];
    const formData = new FormData();
    formData.append('image', file);
    setUploading(true);

    try {
      const config = {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      };

      const { data } = await axios.post('api/upload', formData, config);
      setImage(data);
      setUploading(false);
    } catch (error) {
      console.log(error);
      setUploading(false);
    }
  };

  const submitHandler = e => {
    e.preventDefault();
    dispatch(
      updateProduct({
        _id: productId,
        name,
        price,
        image,
        brand,
        category,
        countInStock,
        description,
        writer,
      }),
    );
  };

  return (
    <>
      <Link to="/admin/productlist" className="btn btn-light my-3">
        Geri Dön
      </Link>
      <FormContainer>
        <h1>Ürün Düzenle</h1>
        {loadingUpdate && <Loader />}
        {errorUpdate && <Message variant="danger">{errorUpdate}</Message>}
        {loading ? (
          <Loader />
        ) : error ? (
          <Message variant="danger">{error}</Message>
        ) : (
          <>
            <Meta title={`Düzenle | ${name}`} />
            <Form onSubmit={submitHandler}>
              <Form.Group controlId="name">
                <Form.Label>İsim</Form.Label>
                <Form.Control type="name" placeholder="İsim giriniz" value={name} onChange={e => setName(e.target.value)}></Form.Control>
              </Form.Group>
              <Form.Group controlId="price">
                <Form.Label>Fiyat</Form.Label>
                <Form.Control type="number" placeholder="Fiyat giriniz." value={price} onChange={e => setPrice(e.target.value)}></Form.Control>
              </Form.Group>
              <Form.Group controlId="image">
                <Form.Label>Resim</Form.Label>
                <Form.Control type="text" placeholder="Resim yükleyiniz." value={image} onChange={e => setImage(e.target.value)}></Form.Control>
                <Form.File id="image-file" label="Dosya Seçin" custom onChange={uploadFileHandler}></Form.File>
                {uploading && <Loader />}
              </Form.Group>
              <Form.Group controlId="brand">
                <Form.Label>Marka</Form.Label>
                <Form.Control type="text" placeholder="Marka girin." value={brand} onChange={e => setBrand(e.target.value)}></Form.Control>
              </Form.Group>
              <Form.Group controlId="category">
                <Form.Label>Kategori</Form.Label>
                <Form.Control as="select" onChange={e => setCategory(e.target.value)} value={category}>
                  <option value="Anı">Anı</option>
                  <option value="Anlatı">Anlatı</option>
                  <option value="Çizgi Roman">Çizgi Roman</option>
                  <option value="Deneme">Deneme</option>
                  <option value="Destan">Destan</option>
                  <option value="Eleştiri">Eleştiri</option>
                  <option value="Halk Edebiyatı">Halk Edebiyat</option>
                  <option value="Roman">Roman</option>
                  <option value="Hiciv">Hiciv</option>
                </Form.Control>
              </Form.Group>
              <Form.Group controlId="imacountInStockge">
                <Form.Label>Stok sayısı</Form.Label>
                <Form.Control type="number" placeholder="Stok sayısını giriniz." value={countInStock} onChange={e => setCountInStock(e.target.value)}></Form.Control>
              </Form.Group>
              <Form.Group controlId="description">
                <Form.Label>Açıklama</Form.Label>
                <Form.Control type="text" placeholder="Açıklama giriniz." value={description} onChange={e => setDescription(e.target.value)}></Form.Control>
              </Form.Group>
              <Form.Group controlId="description">
                <Form.Label>Yazar</Form.Label>
                <Form.Control type="text" placeholder="Yazar ismini giriniz" value={writer} onChange={e => setWriter(e.target.value)}></Form.Control>
              </Form.Group>
              <Button type="submit" variant="primary">
                Güncelle
              </Button>
            </Form>
          </>
        )}
      </FormContainer>
    </>
  );
};

export default ProductEditScreen;
