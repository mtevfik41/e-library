import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import FormContainer from '../components/FormContainer';
import CheckoutSteps from '../components/CheckoutSteps';
import Meta from '../components/Meta';
import { saveShippingAddress } from '../actions/cart-actions';

const ShippingScreen = ({ history }) => {
  const cart = useSelector(state => state.cart);
  const { shippingAddress } = cart;

  const [address, setAddress] = useState(shippingAddress.address);
  const [city, setCity] = useState(shippingAddress.city);
  const [postalCode, setPostalCode] = useState(shippingAddress.postalCode);
  const [country, setCountry] = useState(shippingAddress.country);

  const dispatch = useDispatch();

  const submitHandler = e => {
    e.preventDefault();
    dispatch(saveShippingAddress({ address, city, postalCode, country }));
    history.push('/payment');
  };

  return (
    <FormContainer>
      <Meta title="Kargo" />
      <CheckoutSteps step1 step2 />
      <h1>Kargo</h1>
      <Form onSubmit={submitHandler}>
        <Form.Group controlId="address">
          <Form.Label>Adres</Form.Label>
          <Form.Control type="text" placeholder="Adres giriniz" value={address} required onChange={e => setAddress(e.target.value)}></Form.Control>
        </Form.Group>
        <Form.Group controlId="city">
          <Form.Label>Şehir</Form.Label>
          <Form.Control type="text" placeholder="Şehir giriniz" value={city} required onChange={e => setCity(e.target.value)}></Form.Control>
        </Form.Group>
        <Form.Group controlId="postalCode">
          <Form.Label>Posta kodu</Form.Label>
          <Form.Control type="text" placeholder="Posta kodunu giriniz" value={postalCode} required onChange={e => setPostalCode(e.target.value)}></Form.Control>
        </Form.Group>
        <Form.Group controlId="country">
          <Form.Label>Ülke</Form.Label>
          <Form.Control type="text" placeholder="Ülke giriniz" value={country} required onChange={e => setCountry(e.target.value)}></Form.Control>
        </Form.Group>
        <Button type="submit" variant="primary">
          Devam et
        </Button>
      </Form>
    </FormContainer>
  );
};

export default ShippingScreen;
