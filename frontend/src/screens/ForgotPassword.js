import React, { useState, useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Form, Button, Row, Col } from 'react-bootstrap';
import Message from '../components/Message';
import Loader from '../components/Loader';
import Meta from '../components/Meta';
import FormContainer from '../components/FormContainer';
import axios from 'axios';

const ForgotPassword = () => {
  const [email, setEmail] = useState('');
  const [showMessage, setShowMessage] = useState(false);

  const submitHandler = async e => {
    e.preventDefault();
    const body = {email};
    const res = await axios.post("api/users/forgot-password", body);
    console.log(res.data)
    setShowMessage(true);
  };

  return (
    <FormContainer>
      <Meta title="Şifremi Unuttum" />
      <h1>Şifremi Unuttum</h1>
      {showMessage &&
        <Fragment>
          <Row className="py-3">
            <Col>
              Şifreniz mail adresinize gönderildi. Lütfen mail adresinizi kontrol ediniz.
            </Col>
          </Row>
        </Fragment>
      }
      <Form onSubmit={submitHandler}>
        <Form.Group controlId="email">
          <Form.Label>Email</Form.Label>
          <Form.Control type="email" placeholder="Email giriniz" value={email} onChange={e => setEmail(e.target.value)}></Form.Control>
        </Form.Group>
        <Button type="submit" variant="primary">
          Şifremi Unuttum
        </Button>
      </Form>

      <Row className="py-3">
        <Col>
          <Link to="login">Giriş Yap</Link>
        </Col>
      </Row>
    </FormContainer>
  );
};

export default ForgotPassword;
